package com.adhoc.samplescreen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ExpandableListView;

import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Gson gson = new Gson();
        OfficeListModel officeListModel = gson.fromJson(Constants.inputData, OfficeListModel.class);

        ExpandableListView expandableListView = findViewById(R.id.expandableListView);
        ExpandableListViewAdapter expandableListViewAdapter = new ExpandableListViewAdapter(this, officeListModel.getOfficeDetails());
        expandableListView.setAdapter(expandableListViewAdapter);

        Log.i("MainActivity", "officeListModel " + officeListModel.toString());
    }
}
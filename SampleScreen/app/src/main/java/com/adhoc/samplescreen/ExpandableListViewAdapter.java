package com.adhoc.samplescreen;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import java.util.ArrayList;

public class ExpandableListViewAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<OfficeModel> listOfficeDetails;

    public ExpandableListViewAdapter(Context context, ArrayList<OfficeModel> listOfficeDetails) {
        this.context = context;
        this.listOfficeDetails = listOfficeDetails;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.listOfficeDetails.get(groupPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groupPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final OfficeModel officeModel = (OfficeModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_row_child, null);
        }

        TextView textViewChild = convertView.findViewById(R.id.textViewChild);
        if (childPosition == 0) {
            textViewChild.setText(officeModel.getOffice_address_1() + "\n" + officeModel.getOffice_address_2());
            textViewChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?daddr=" + officeModel.getLatitute() + "," + officeModel.getLongitute()));
                    context.startActivity(intent);
                }
            });
        } else if (childPosition == 1) {
            textViewChild.setText("Call " + officeModel.getOffice_phone());
            textViewChild.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Permissions.check(context, Manifest.permission.CALL_PHONE, null, new PermissionHandler() {
                        @Override
                        public void onGranted() {
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + officeModel.getOffice_phone()));
                            context.startActivity(intent);
                        }
                    });
                }
            });
        } else if (childPosition == 2) {
            textViewChild.setText("Email " + officeModel.getOffice_email());
            textViewChild.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, officeModel.getOffice_email());
                    context.startActivity(intent);
                }
            });

        } else if (childPosition == 3) {
            textViewChild.setText("Website " + officeModel.getOffice_website());
            textViewChild.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("http://" + officeModel.getOffice_website()));
                    i.addCategory(Intent.CATEGORY_BROWSABLE);
                    context.startActivity(i);
                }
            });
        } else if (childPosition == 4) {
            textViewChild.setText("Facebook " + officeModel.getOffice_facebook());
            textViewChild.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(officeModel.getOffice_facebook()));
                    context.startActivity(intent);
                }
            });
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 5;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listOfficeDetails.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listOfficeDetails.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        OfficeModel officeModel = (OfficeModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_row_group, null);
        }
        TextView textViewGroup = convertView
                .findViewById(R.id.textViewGroup);
        textViewGroup.setTypeface(null, Typeface.BOLD);
        textViewGroup.setText(officeModel.getOffice_name());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
package com.adhoc.samplescreen;

public class OfficeModel {
    String office_name;
    String office_address_1;
    String office_address_2;
    String office_phone;
    String office_fax;
    String office_email;
    String office_website;
    String office_facebook;
    String Latitute;
    String Longitute;

    public String getOffice_name() {
        return office_name;
    }

    public void setOffice_name(String office_name) {
        this.office_name = office_name;
    }

    public String getOffice_address_1() {
        return office_address_1;
    }

    public void setOffice_address_1(String office_address_1) {
        this.office_address_1 = office_address_1;
    }

    public String getOffice_address_2() {
        return office_address_2;
    }

    public void setOffice_address_2(String office_address_2) {
        this.office_address_2 = office_address_2;
    }

    public String getOffice_phone() {
        return office_phone;
    }

    public void setOffice_phone(String office_phone) {
        this.office_phone = office_phone;
    }

    public String getOffice_fax() {
        return office_fax;
    }

    public void setOffice_fax(String office_fax) {
        this.office_fax = office_fax;
    }

    public String getOffice_email() {
        return office_email;
    }

    public void setOffice_email(String office_email) {
        this.office_email = office_email;
    }

    public String getOffice_website() {
        return office_website;
    }

    public void setOffice_website(String office_website) {
        this.office_website = office_website;
    }

    public String getOffice_facebook() {
        return office_facebook;
    }

    public void setOffice_facebook(String office_facebook) {
        this.office_facebook = office_facebook;
    }

    public String getLatitute() {
        return Latitute;
    }

    public void setLatitute(String latitute) {
        Latitute = latitute;
    }

    public String getLongitute() {
        return Longitute;
    }

    public void setLongitute(String longitute) {
        Longitute = longitute;
    }
}

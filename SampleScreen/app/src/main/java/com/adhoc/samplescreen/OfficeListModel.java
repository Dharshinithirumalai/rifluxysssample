package com.adhoc.samplescreen;

import java.util.ArrayList;

public class OfficeListModel {
    public ArrayList<OfficeModel> getOfficeDetails() {
        return OfficeDetails;
    }

    public void setOfficeDetails(ArrayList<OfficeModel> officeDetails) {
        OfficeDetails = officeDetails;
    }

    ArrayList<OfficeModel> OfficeDetails = new ArrayList<>();
}

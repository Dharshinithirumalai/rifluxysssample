package com.adhoc.samplescreen;

public class Constants {
    public static String inputData = "{\n" +
            "\t\"OfficeDetails\": [\n" +
            "\t\t{\n" +
            "\t\t\t\"office_name\": \"Rifluxyss Softwares PVT LTD\",\n" +
            "\t\t\t\"office_address_1\": \"Five Furlong Rd, Maduvinkarai, Guindy\",\n" +
            "\t\t\t\"office_address_2\": \"Chennai, Tamil Nadu 600032\",\n" +
            "\t\t\t\"office_phone\": \"P: 044 2255 3380\",\n" +
            "\t\t\t\"office_fax\": \"F: (044) 2770 2112\",\n" +
            "\t\t\t\"office_email\": \"support@rifluxyss.com\",\n" +
            "\t\t\t\"office_website\": \"www.rifluxyss.com\",\n" +
            "\t\t\t\"office_facebook\": \"https://www.facebook.com/pages/category/Business-Service/Rifluxyss-software-1660079617566638/\",\n" +
            "\t\t\t\"Latitute\": \"12.9998531\",\n" +
            "\t\t\t\"Longitute\": \"80.2119642\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"office_name\": \"Phoenix Marketcity\",\n" +
            "\t\t\t\"office_address_1\": \"142, Velachery Main Rd, Indira Gandhi Nagar\",\n" +
            "\t\t\t\"office_address_2\": \"Velachery, Chennai, Tamil Nadu 600042\",\n" +
            "\t\t\t\"office_phone\": \"P: 044 6134 3008\",\n" +
            "\t\t\t\"office_fax\": \"\",\n" +
            "\t\t\t\"office_email\": \"\",\n" +
            "\t\t\t\"office_website\": \"https://www.phoenixmarketcity.com/chennai\",\n" +
            "\t\t\t\"office_facebook\": \"\",\n" +
            "\t\t\t\"Latitute\": \"12.990511\",\n" +
            "\t\t\t\"Longitute\": \"80.2147877\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"office_name\": \"Forum Vijaya Mall\",\n" +
            "\t\t\t\"office_address_1\": \"183, Great Southern Trunk Rd, Arcot Rd\",\n" +
            "\t\t\t\"office_address_2\": \"Vadapalani, Chennai, Tamil Nadu 600026\",\n" +
            "\t\t\t\"office_phone\": \"P: 044 4904 9000\",\n" +
            "\t\t\t\"office_fax\": \"\",\n" +
            "\t\t\t\"office_email\": \"\",\n" +
            "\t\t\t\"office_website\": \"\",\n" +
            "\t\t\t\"office_facebook\": \"The Forum Vijaya Mall\",\n" +
            "\t\t\t\"Latitute\": \"13.0498937\",\n" +
            "\t\t\t\"Longitute\": \"80.2095018\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"office_name\": \"VR Chennai\",\n" +
            "\t\t\t\"office_address_1\": \"Jawaharlal Nehru Rd, Thirumangalam, Anna Nagar\",\n" +
            "\t\t\t\"office_address_2\": \"Chennai, Tamil Nadu 600040\",\n" +
            "\t\t\t\"office_phone\": \"P: 044 6670 5555\",\n" +
            "\t\t\t\"office_fax\": \"\",\n" +
            "\t\t\t\"office_email\": \"\",\n" +
            "\t\t\t\"office_website\": \"www.vrchennai.com\",\n" +
            "\t\t\t\"office_facebook\": \"\",\n" +
            "\t\t\t\"Latitute\": \"13.0805465\",\n" +
            "\t\t\t\"Longitute\": \"80.197132\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"office_name\": \"Ampa Skywalk\",\n" +
            "\t\t\t\"office_address_1\": \"No:1, Nelson Manickam Road, 627, Poonamalee High Road\",\n" +
            "\t\t\t\"office_address_2\": \"Aminjikarai, Chennai, Tamil Nadu 600029\",\n" +
            "\t\t\t\"office_phone\": \"P: 044 3024 9494\",\n" +
            "\t\t\t\"office_fax\": \"\",\n" +
            "\t\t\t\"office_email\": \"\",\n" +
            "\t\t\t\"office_website\": \"www.ampaskywalk.com\",\n" +
            "\t\t\t\"office_facebook\": \"\",\n" +
            "\t\t\t\"Latitute\": \"13.0734711\",\n" +
            "\t\t\t\"Longitute\": \"80.2214432\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"office_name\": \"Spencer Plaza\",\n" +
            "\t\t\t\"office_address_1\": \"768-769, Anna Salai\",\n" +
            "\t\t\t\"office_address_2\": \" Chennai, Tamil Nadu 600002\",\n" +
            "\t\t\t\"office_phone\": \"P: 044 2849 1001\",\n" +
            "\t\t\t\"office_fax\": \"F: (044) 7700 2121\",\n" +
            "\t\t\t\"office_email\": \"\",\n" +
            "\t\t\t\"office_website\": \"www.spencerplaza.co.in\",\n" +
            "\t\t\t\"office_facebook\": \"\",\n" +
            "\t\t\t\"Latitute\": \"13.0615102\",\n" +
            "\t\t\t\"Longitute\": \"80.2614606\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"office_name\": \"Express Avenue\",\n" +
            "\t\t\t\"office_address_1\": \"Pattullos Rd, Express Estate, Thousand Lights\",\n" +
            "\t\t\t\"office_address_2\": \"Chennai, Tamil Nadu 600002\",\n" +
            "\t\t\t\"office_phone\": \"P: 044 2846 4646\",\n" +
            "\t\t\t\"office_fax\": \"\",\n" +
            "\t\t\t\"office_email\": \"\",\n" +
            "\t\t\t\"office_website\": \"www.expressavenue.in\",\n" +
            "\t\t\t\"office_facebook\": \"\",\n" +
            "\t\t\t\"Latitute\": \"13.0584546\",\n" +
            "\t\t\t\"Longitute\": \"80.2641844\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"office_name\": \"Chandra Metro Mall\",\n" +
            "\t\t\t\"office_address_1\": \"262, Arcot Rd, Rajeswari Colony, Virugambakkam\",\n" +
            "\t\t\t\"office_address_2\": \"Chennai, Tamil Nadu 600092\",\n" +
            "\t\t\t\"office_phone\": \"P: 097899 37575\",\n" +
            "\t\t\t\"office_fax\": \"F: (044) 2088 3301\",\n" +
            "\t\t\t\"office_email\": \"\",\n" +
            "\t\t\t\"office_website\": \"www.chandrabuilders.in\",\n" +
            "\t\t\t\"office_facebook\": \"\",\n" +
            "\t\t\t\"Latitute\": \"13.0470824\",\n" +
            "\t\t\t\"Longitute\": \"80.1903044\"\n" +
            "\t\t}\n" +
            "\t]\n" +
            "}";
}
